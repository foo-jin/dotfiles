#!/bin/fish

for f in packages/*
  sort -u $f -o $f
  git add $f
end
