function ya
    set -l tmp (mktemp -t "yazi-cwd.XXXXX")
	yazi --cwd-file=$tmp
    set -l cwd (cat -- $tmp)
	if test -d $cwd; and not test $cwd -ef $PWD
        echo "cd-ing!"
		cd  -- $cwd
	end
	rm -f -- "$tmp"
end
