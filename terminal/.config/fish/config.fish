abbr -a -g c cargo
abbr -a -g m make
abbr -a -g j just

abbr -a -g g git
abbr -a -g gc 'git checkout'
abbr -a -g gah 'git stash; and git pull --rebase; and git stash pop'
abbr -a -g gfmt 'git test fix --exec "cargo fmt --all"'
abbr -a -g gfwl 'git push --force-with-lease'
abbr -a -g gn 'git next'
abbr -a -g gp 'git prev'

abbr -a -g vimdiff 'nvim -d'
abbr -a -g s! 'sudo !!'
abbr -a -g yt youtube-dl
abbr -a -g sys systemctl
complete --command yay --wraps pacman
abbr -a -g pu pueue
abbr -a -g tar bsdtar
abbr -a -g lg ledger
abbr -a -g pj pijul
abbr -a -g tf terraform

set -l cfg $HOME/.config

set -x PATH /usr/local/bin/ $PATH
set -x PATH $PATH ~/.local/bin
set -x PATH $PATH ~/.cargo/bin
set -x PATH $HOME/.nix-profile/bin $PATH

set -x TZ 'Europe/Amsterdam'
set -x CARGO_TARGET_DIR $HOME/.cache/cargo
set -x RIPGREP_CONFIG_PATH $cfg/ripgreprc
set -x GNUPGHOME $cfg/gnupg
set -x PASSWORD_STORE_GENERATED_LENGTH 20

# source local config
set -l localcfg $cfg/fish/local-config.fish
if test -e $localcfg
    source $localcfg
end

# linux only?
set -x XDG_CONFIG_HOME $HOME/.config
set -l userdirs $cfg/user-dirs.dirs
if test -e $userdirs
    # source the xdg dirs
    awk 'BEGIN { FS = "=" } !/^#/ { printf("set -x %s %s\n", $1, $2) }' $cfg/user-dirs.dirs | source
end

if test -z "$SSH_CLIENT"
    # Start or re-use a gpg-agent.
    gpgconf --launch gpg-agent

    # Ensure that GPG Agent is used as the SSH agent
    set -e SSH_AUTH_SOCK
    set -x SSH_AUTH_SOCK (gpgconf --list-dirs agent-ssh-socket)
end

if type -q exa
    abbr -a -g l exa
    alias ls 'exa -lh --git'
    alias lls 'exa -lha --git'
else
    alias l ls
    alias ls 'ls -l'
    alias lls 'ls -la'
end

if type -q btm
	alias top btm
end

if type -q rg
    set -l rgcmd 'rg --files --hidden --follow --glob "!.git/*"'
    if type -q sk
        set -x SKIM_DEFAULT_COMMAND $rgcmd
    end
end

function remote_alacritty
    # https://gist.github.com/costis/5135502
    set fn (mktemp)
    infocmp alacritty-256color > $fn
    scp $fn $argv[1]":alacritty-256color.ti"
    ssh $argv[1] tic "alacritty-256color.ti"
    ssh $argv[1] rm "alacritty-256color.ti"
end

if type -q nvim
    set -x SUDO_EDITOR (which nvim)
end

if type -q emacs
    set -x EDITOR emacs -nw -Q
    set -x SYSTEMD_EDITOR $EDITOR
	# overwrite
	set -x SUDO_EDITOR $EDITOR
end

if type -q helix
	alias hx helix
	# overwrite
	set -x EDITOR (which helix)
    set -x SYSTEMD_EDITOR $EDITOR
	set -x SUDO_EDITOR $EDITOR
else if type -q hx
	# overwrite
	set -x EDITOR (which hx)
    set -x SYSTEMD_EDITOR $EDITOR
	set -x SUDO_EDITOR $EDITOR
end

if status is-interactive
    if type -q starship
        starship init fish | source
    end

  	if type -q atuin
   	    set -gx ATUIN_NOBIND "true"
	  	atuin init fish | source

	  	# bind to ctrl-r in normal and insert mode, add any other bindings you want here too
	  	bind \cr _atuin_search
	  	bind -M insert \cr _atuin_search
   	end

	if type -q direnv
		direnv hook fish | source
	end

    if type -q jj
        jj util completion --fish | source
    end

    if type -q zoxide
        alias z zoxide
        zoxide init fish | source
    end

    # if type -q zellij
    #     set -x ZELLIJ_AUTO_ATTACH true
    #     set -x ZELLIJ_AUTO_EXIT true
    #     eval (zellij setup --generate-auto-start fish | string collect)
    # end
end
