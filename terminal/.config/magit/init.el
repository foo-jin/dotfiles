;;; init.el --- Magit-specific emacs config -*- lexical-binding: t -*-
;;; Commentary:
;;;     Pruned down version of my emacs config to comfortably run magit.
;;; Code:
(setq debug-on-error nil)

(setq straight-repository-branch "develop")
(defvar bootstrap-version)
(let ((bootstrap-file
	   (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
	  (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
	(with-current-buffer
		(url-retrieve-synchronously
		 "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
		 'silent 'inhibit-cookies)
	  (goto-char (point-max))
	  (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)
(use-package straight
  :custom (straight-use-package-by-default t))

(defconst private-dir (expand-file-name "private/" user-emacs-directory))
(defconst temp-dir (expand-file-name "emacs/" "~/.cache")) ;; expand ~

(setq-default initial-scratch-message ""
			  tab-width 4)
(fset 'yes-or-no-p 'y-or-n-p)
(setq confirm-nonexistent-file-or-buffer t
	  save-interprogram-paste-before-kill t
	  mouse-yank-at-point t
	  visible-bell nil
	  ring-bell-function 'ignore
	  custom-file (expand-file-name "custom.el" private-dir)
	  cursor-in-non-selected-windows nil
	  highlight-nonselected-windows nil
	  inhibit-startup-message t
	  fringes-outside-margins t
	  select-enable-clipboard t
	  inhibit-x-resources t
	  sentence-end-double-space nil)
;; http://ergoemacs.org/emacs/emacs_stop_cursor_enter_prompt.html
(setq
 minibuffer-prompt-properties
 '(read-only t point-entered minibuffer-avoid-prompt face minibuffer-prompt))

;; ensure "auto-save-list" folder exists
(unless (file-exists-p (expand-file-name "auto-save-list" temp-dir))
  (make-directory (expand-file-name "auto-save-list/" temp-dir) :parents))
(set-face-attribute 'default nil
					:family "Source Code Pro"
					:height 110
					:width 'normal
					:weight 'normal)

(add-hook 'before-save-hook 'delete-trailing-whitespace)
(menu-bar-mode -1)
(when (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(when (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
(when (fboundp 'save-place-mode)
  (setq save-place-file (expand-file-name "places" temp-dir))
  (save-place-mode +1)
  (setq-default save-place t))
(blink-cursor-mode)
(show-paren-mode 1)

;; github.com/magit/magit
(use-package magit
  :bind ("C-c g" . 'magit-file-dispatch)
  :config
  (setq magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

(use-package doom-themes
  :init
  (setq custom-safe-themes t)
  (load-theme 'doom-homage-white)
  :config
  (setq doom-themes-enable-bold t
		doom-themes-enable-italic t))

(use-package which-key
  :delight
  :config
  (which-key-mode))

(use-package exec-path-from-shell
  :config
  (when (daemonp)
	(setq exec-path-from-shell-variables
		  '("PATH" "MANPATH" "SSH_AUTH_SOCK" "GNUPGHOME")))
  (exec-path-from-shell-initialize))

(autoload 'View-scroll-half-page-forward "view")
(autoload 'View-scroll-half-page-backward "view")

;; (global-set-key (kbd "[SHORTCUT]") '[FUNCTION])
(global-set-key (kbd "C-x C-b") #'ibuffer)
(global-set-key (kbd "C-v") 'View-scroll-half-page-forward)
(global-set-key (kbd "M-v") 'View-scroll-half-page-backward)

(global-set-key (kbd "C-a") 'move-beginning-of-line-or-indentation)
(global-set-key (kbd "C-e") 'move-end-of-line-or-indentation)

(global-set-key (kbd "M-g w") 'browse-url)
(global-set-key (kbd "C-x |") 'toggle-window-split)

(use-package ace-window
  :bind ("M-o" . 'ace-window)
  :config
  (custom-set-faces '(aw-leading-char-face ((t (:foreground "cyan" :height 3.0)))))
  (setq aw-keys '(?a ?r ?s ?t ?m ?n ?e ?i ?o)
		aw-scope 'global
		aw-minibuffer-flag t
		aw-dispatch-always t))

(use-package selectrum
  :config
  (selectrum-mode +1))

(use-package prescient
  :config
  (setq prescient-save-file (expand-file-name "prescient-save.el" private-dir))
  (prescient-persist-mode +1))

(use-package selectrum-prescient
  :config
  (setq selectrum-prescient-enable-filtering nil)
  (selectrum-prescient-mode +1))

(setq tab-always-indent 'complete)
(setq tab-first-completion 'eol)

(use-package corfu
  :custom
  (corfu-quit-at-boundary 'separator)
  (corfu-quit-no-match 't)
  (corfu-echo-documentation t)
  (corfu-cycle t)
  (corfu-preselect 'first)
  (corfu-auto t)
  (corfu-auto-delay 0)
  (corfu-auto-prefix 1)
  :bind
  (:map corfu-map ("C-g" . 'corfu-quit))
  :init
  (global-corfu-mode))

(use-package cape
  :init
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-file))

;; custom defuns
(defun switch-to-minibuffer-window ()
  "Switch to minibuffer window (if active)."
  (interactive)
  (when (active-minibuffer-window)
	(select-window (active-minibuffer-window))))

(defun toggle-window-split ()
  (interactive)
  (if (= (count-windows) 2)
	  (let* ((this-win-buffer (window-buffer))
			 (next-win-buffer (window-buffer (next-window)))
			 (this-win-edges (window-edges (selected-window)))
			 (next-win-edges (window-edges (next-window)))
			 (this-win-2nd (not (and (<= (car this-win-edges)
										 (car next-win-edges))
									 (<= (cadr this-win-edges)
										 (cadr next-win-edges)))))
			 (splitter
			  (if (= (car this-win-edges)
					 (car (window-edges (next-window))))
				  'split-window-horizontally
				'split-window-vertically)))
		(delete-other-windows)
		(let ((first-win (selected-window)))
		  (funcall splitter)
		  (if this-win-2nd (other-window 1))
		  (set-window-buffer (selected-window) this-win-buffer)
		  (set-window-buffer (next-window) next-win-buffer)
		  (select-window first-win)
		  (if this-win-2nd (other-window 1))))))

;;; init.el ends here
